package mvc.views.utils;

import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import mvc.models.Modelo;


public class Alerta {
    
    private Alerta() {}
    
    public static final byte CONFIRMAR_ELIMINAR = 0;
    public static final byte CONFIRMAR_ACTUALIZAR = 1;
    public static final byte MENSAJE_ERROR = 2;
    public static final byte MENSAJE = 3;
    
    public static int confimacion(JComponent padre, Modelo modelo, byte tipo){
        int eleccion = 0;
        String msj;
        switch(tipo){
            case CONFIRMAR_ELIMINAR:
                msj = "Se eliminará: \n" + modelo.muestraAtributos();
                eleccion =  JOptionPane.showConfirmDialog(padre, 
                        msj,
                        "¡Cuidado!",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.DEFAULT_OPTION,
                        crearIcono("img/cleanKirby.png")
                );
                break;
            case CONFIRMAR_ACTUALIZAR:
                msj = "Se actualizará: \n" + modelo.muestraAtributos();
                eleccion =  JOptionPane.showConfirmDialog(padre, 
                        msj,
                        "¡Cuidado!",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.DEFAULT_OPTION,
                        crearIcono("img/cleanKirby.png")
                );
                break;
        }

        return eleccion;
    }
    
    public static void informacion(
            JComponent padre, Modelo modelo, int tipo, String msj){

        switch(tipo){
            case MENSAJE_ERROR:
                JOptionPane.showMessageDialog(padre,
                        msj,
                        "Información",
                        JOptionPane.ERROR_MESSAGE,
                        crearIcono("img/Sad_kirby.png")
                );
                break;
            case MENSAJE:
                JOptionPane.showMessageDialog(padre,
                        msj,
                        "Información",
                        JOptionPane.INFORMATION_MESSAGE,
                        crearIcono("img/happy_kirby.png")
                );
                break;
        }
            
    }

    /**
     * crea un ícono a partir de un path dado
     *
     * @param path
     * @return Icon icono.
     */
    public static Icon crearIcono(String path) {
        // instanciamos la imagen.
        ImageIcon img = new ImageIcon(path);
        // creamos la imágen pura desde ImageIcon:
        Image imagen = img.getImage().getScaledInstance(
                70,
                70,
                Image.SCALE_SMOOTH
        );
        // usamos el constructor de ImageIcon que tiene un Image como parámetro.
        Icon icono = new ImageIcon(imagen);
        return icono;
    }
    
}
